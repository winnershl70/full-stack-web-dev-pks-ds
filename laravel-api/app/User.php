<?php
namespace App;
use App\roles;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\OtpCode;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends  Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = ['username','email','name','roles_id','password','email_verified_at'];

    protected $primaryKey= 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()}= Str::uuid();
            }
            $model->roles_id = roles::where('name','admin')->first()->id;
        });
    }

    public function roles()
    {
      return $this->belongsTo('App\roles');
    }
    public function otp_code()
    {
     return $this->hasOne('App\OtpCode');
    }  

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}


