<?php

namespace App\Events;

use App\comments;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentStoredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comments;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(comments $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
   
}
