<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use Illuminate\Support\Facades\Validator;
class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table posts
        $posts = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $posts  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }



        //save to database
        $post = Post::create([
            'title'     => $request->title,
            'description'   => $request->description
        ]);

        //success save to database
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $post  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //find post by ID
       $post = Post::findOrfail($id);

       //make response JSON
       return response()->json([
           'success' => true,
           'message' => 'Detail Data Post',
           'data'    => $post 
       ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $allRequest = $request->all();
                //set validation
       $validator = Validator::make($request->all(), [
        'title'   => 'required',
        'description' => 'required',
    ]);
    
    //response error validation
    if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
    }

    //find post by ID
    $post = Post::findOrFail($post->id);

   

    if($post) {

        $user = auth()->user();

        if($post->user_id !=$user->id){

            return response()->json([
                'success' => false,
                'message' => 'data post ini bukan milik user '
            ], 403);

        }
        //update post
        $post->update([
            'title'     => $request->title,
            'description'   => $request->description
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Post Updated',
            'data'    => $post  
        ], 200);

    }

    //data post not found
    return response()->json([
        'success' => false,
        'message' => 'Post Not Found',
    ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrfail($id);

        if($post) {
            $user = auth()->user();
            if($post->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'data post ini bukan milik user '
                ], 403);
    
            }
            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
