<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserOtpStoredEvent;
use App\users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'email' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = users::where('email', $request->email)->first();
     
        
        if($users->otp_codes){
           $users->otp_codes->delete();  
        }          
        
        do {
            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        
        $now = Carbon::now();
        $otp_code=OtpCode::create([                     
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $users->id
        ]);

        event(new UserOtpStoredEvent($otp_code)); 
         //success save to database
             return response()->json([
                 'success' => true,
                 'message' => 'otp di genertae',
                 'data'    => [
                     'user' => $users,
                     'otp_code' => $otp_code
                 ]  
             ], 201);

    }
}
