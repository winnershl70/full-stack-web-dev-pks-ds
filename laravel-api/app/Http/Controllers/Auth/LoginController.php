<?php

namespace App\Http\Controllers\Auth;
use App\users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\AuthenticationException;
class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'email' => 'required',
            'password' => 'required'
        ]);

          //response error validation
          if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'success' => false,
                'message' => 'email & password tidak ditemukan'
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'berhasil login',
            'data' =>[
            'user' => auth()->User(),
            'token' => $token
            ]
        ]);

    }
}
