<?php

namespace App\Http\Controllers\Auth;

use App\users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('masuk');
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'email'=> 'required',
            'password'=> 'required|confirmed|min:6',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = users::where('email', $request->email)->first();

       
        if(!$user){
            return response()->json([
                'success' => false,
                'message' => 'email tidak ada'
            ], 400); 
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'password sudah di update',
            'data' => $user
        ]); 
        // return response()->json()([
        //     'success' => true,
        //     'message' => 'password berhasil di ubah',
        //     'data' => $user             
        // ]);

    }
}
