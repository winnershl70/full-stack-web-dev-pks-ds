<?php

namespace App\Http\Controllers\Auth;

use App\users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserOtpStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' =>'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = users::create($allRequest);


       do {
           $random = mt_rand(100000 , 999999);
           $check = OtpCode::where('otp', $random)->first();
       } while ($check);

       $now = Carbon::now();
       $otp_code=OtpCode::create([
           'otp' => $random,
           'valid_until' => $now->addMinutes(5),
           'user_id' => $user->id

       ]);
       event(new UserOtpStoredEvent($otp_code)); 
        //success save to database
            return response()->json([
                'success' => true,
                'message' => 'suda idi register',
                'data'    => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ]  
            ], 201);


        //failed save to database
        
    }
}
