<?php

namespace App\Mail;

use App\comments;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentAuthormail extends Mailable
{
    use Queueable, SerializesModels;

    public $comments;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(comments $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comment.comment_author_mail');
    }
}
