<?php

namespace App\Listeners;

use App\Mail\RegenerateEmailUser;
use App\Events\UserOtpStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserOtpStoredEvent  $event
     * @return void
     */
    public function handle(UserOtpStoredEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerateEmailUser($event->otp_code));
    }
}
