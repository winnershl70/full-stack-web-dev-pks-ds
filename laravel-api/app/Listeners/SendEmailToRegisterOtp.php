<?php

namespace App\Listeners;

use App\Events\UserOtpStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegisterOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserOtpStoredEvent  $event
     * @return void
     */
    public function handle(UserOtpStoredEvent $event)
    {
        //
    }
}
