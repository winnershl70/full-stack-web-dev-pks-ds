//soal no 1

const luas = (panjang, lebar) =>{
    return panjang*lebar;
}
const keliling = (panjang , lebar)=>{
    return(panjang+lebar) *2
}
console.log(luas(3, 4)) ;
console.log(keliling(3, 4)) ;

//soal no 2
const newFunction = (firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName: ()=>{
        console.log(firstName + " " + lastName)
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

//soal no 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
//jawaban soal no 3
  const {firstName, lastName, address , hobby}= newObject

  console.log(firstName, lastName, address, hobby)

//soal no 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

//soal no 5 
const planet = "earth" 
const view = "glass" 
var before = `Lorem  ${view}  dolor sit amet,  consectetur adipiscing elit, ${planet}`
console.log(before) 