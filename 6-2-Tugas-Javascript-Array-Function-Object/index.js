
//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]

daftarHewan.sort()
daftarHewan.forEach(function (hewan) {
   console.log(hewan)
})

// soal 2

 
function introduce(isi){
 return "Nama saya " +isi.name+", umur saya "+isi.age +" tahun, alamat saya di "+isi.address+", dan saya punya hobby yaitu " + isi.hobby

}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) 

//soal 3
function hitung_huruf_vokal(str) {

    var vokal = str.match(/[aeiou]/gi).length
    return  vokal
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//soal no 4
function hitung(int) {

    var angka = -2
    var i 
        for ( i = 0; i < int; i++) {
            angka += 2
        }
    
    return angka 
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8